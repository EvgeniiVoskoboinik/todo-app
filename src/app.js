angular
    .module('app',[])
    .controller('mainCtrl', mainCtrl)
    .directive('taskContent', taskContent);

mainCtrl.$inject = ['$scope',  '$window'];
function mainCtrl($scope, $window){
    var model = $window.localStorage['TODO_app_model'];

    $scope.model = model ? JSON.parse(model) : {
        tasks: [],
        new_task: '',
        reverse: true
    };

    $scope.addTask = addTask;
    $scope.removeTask = removeTask;

    $scope.$watch('model', function(n,o){
        $window.localStorage['TODO_app_model'] = JSON.stringify(n);
    }, true);

    function addTask(task){
        task = task.trim();
        if(task === ''){
            return;
        }
        $scope.model.tasks.push({
            created: Date.now(),
            title: task,
            completed: false
        });
        $scope.model.new_task = '';
    }

    function removeTask(task){
        var tasks = $scope.model.tasks;
        for (var i= 0, len= tasks.length; i<len; i++){
            var t = tasks[i];
            if(t.created === task.created){
                tasks.splice(i,1);
                break;
            }
        }
    }
}

taskContent.$inject = ['$timeout'];
function taskContent($timeout){
    return{
        restrict: 'A',
        scope:{
            title: '=',
            edit: '='
        },
        templateUrl: 'taskContent.html',
        link: function(scope, element, attrs){
            var textarea = element[0].children[1];
            var div = element[0].children[0];

            scope.expand = function() {
                var scrollHeight = textarea.scrollHeight;
                textarea.style.height =  scrollHeight + "px";
            };

            scope.preventEnter = function(e){
                if(e.keyCode === 13){
                    e.preventDefault();
                    scope.edit = false;
                    return false;
                }
            };

            scope.$watch('edit', function(newVal,oldVal){
                if(newVal && newVal !== oldVal){
                    textarea.style.height = div.scrollHeight + 'px';
                    $timeout(function(){
                        textarea.focus();
                    },0);
                }
            })
        }
    }
}