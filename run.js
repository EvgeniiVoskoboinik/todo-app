var express = require('express');
var app = express();
var port = 3003;
app.use(express.static(__dirname + '/src'));

var server = require('http').Server(app);
server.listen(port);
console.log('Express app started on port ' + port);